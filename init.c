#define _GNU_SOURCE
#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <sys/mount.h>
#include <sys/reboot.h>

#define HOSTNAME "ungol"

static const struct {
	char *name;
	char *device;
	char *fstype;
	uid_t user;
	gid_t group;
	mode_t mode;
} dirs[] = {
	{ "/dev", "udev", "devtmpfs", 0, 0, 0755 },
	{ "/dev/pts", "devpts", "devpts", 0, 0, 0755 },
	{ "/sys/linux/proc", "proc", "proc", 0, 0, 0755 },
	{ "/sys/linux/sys", "sysfs", "sysfs", 0, 0, 0755 },
	{ "/tmp", "tmpfs", "tmpfs", 0, 0, 01777 },
	{ "/var", NULL, NULL, 0, 0, 0755 },
};

static sig_atomic_t shutdown = 0;
static sig_atomic_t child = 0;

void sighandler(int sig)
{
	switch (sig) {
	case SIGHUP:
		shutdown = RB_AUTOBOOT;
		break;

	case SIGUSR1:
		shutdown = RB_POWER_OFF;
		break;

	case SIGCHLD:
		child = 1;
		break;

	default:
		break;
	}

	signal(sig, sighandler);
}

pid_t start_respawnd(void)
{
	pid_t pid = fork();
	if (pid == 0) {
		execl("/bin/respawnd", "/bin/respawnd", NULL);
		exit(1);
	}
	return pid;
}

int main(int argc, char *argv[])
{
	int c;
	while ((c = getopt(argc, argv, "hr")) != -1) {
		switch (c) {
		case 'r':
			if (kill(1, SIGHUP) != 0) {
				perror("init: reboot");
				return 1;
			}
			break;

		case 'h':
			if (kill(1, SIGUSR1) != 0) {
				perror("init: halt");
				return 1;
			}
			break;

		default:
			break;
		}
	}

	if (getpid() != 1) {
		return 0;
	}

	chdir("/");

	for (size_t i = 0; i < sizeof(dirs) / sizeof(dirs[0]); i++) {
		mkdir(dirs[i].name, dirs[i].mode);
		chown(dirs[i].name, dirs[i].user, dirs[i].group);
		if (dirs[i].fstype) {
			mount(dirs[i].device, dirs[i].name, dirs[i].fstype, 0, NULL);
		}
	}

	signal(SIGHUP, sighandler);
	signal(SIGUSR1, sighandler);
	signal(SIGCHLD, sighandler);

	sethostname(HOSTNAME, sizeof(HOSTNAME));

	pid_t respawn = start_respawnd();
	while (shutdown == 0) {
		sleep(UINT_MAX);
		if (child) {
			int status;
			pid_t reap = wait(&status);
			if (reap == respawn) {
				if (status != 0) {
					printf("something is wrong with respawnd: %d\n", status);
					shutdown = RB_POWER_OFF;
					break;
				}
				respawn = start_respawnd();
			}
		}
	}

	reboot(shutdown);
}
